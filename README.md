### PeerExplorer NXT NRS Client Plugin ###


### Overview ###

PeerExplorer plugin is your gateway for NXTs infrastructure.

### Running software ###

 Copy all files in your NRS client plugin folder and use the given structure. You don't have to replace anything.
 
 - nxt/html/ui/plugins/

### Technical details: ###

The plugin uses the PeerExplorer.com API

### Want to contribute? ###

 - create pull requests
 - review pull requests
 - review existing code
 - create issues
 - answer issues

### Preview: ###
![preview_peerexplorer_plugin.png](https://bitbucket.org/repo/G9nadk/images/578051204-preview_peerexplorer_plugin.png)