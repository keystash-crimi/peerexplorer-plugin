/**
 * PeerExplorer - Plugin
 * @depends {nrs.js}
 */
var NRS = (function (NRS, $, undefined) {
    NRS.p_peerexplorer_connectPeer = function (peer) {
        NRS.sendRequest("addPeer", {"peer": peer}, function (response) {
            if (response.errorCode || response.error || response.state != 1) {
                $.growl($.t("failed_connect_peer"), {
                    "type": "danger"
                });
            } else {
                $.growl($.t("success_connect_peer"), {
                    "type": "success"
                });
            }
            if (NRS.currentPage == 'p_peerexplorer') {
                NRS.loadPage("p_peerexplorer");
            }
        });
    }

    NRS.pages.p_peerexplorer = function () {
        NRS.sendRequest("getPeers+", {
            "active": "true",
            "includePeerInfo": "true"
        }, function (response) {
            if (response.peers && response.peers.length) {
                var rows = "";
                var currentlyOnline = 0;
                var averageOnline = 0;
                var averageResponseTime = 0;
                var version = 0;
                var versionUsed = 0;
                var connectedPeers = new Array();
                var countPeers = 1;
                var versionToCompare = (!NRS.isTestNet ? NRS.normalVersion.versionNr : NRS.state.version);
                for (var i = 0; i < response.peers.length; i++) {
                    var peer = response.peers[i];
                    connectedPeers.push(String(peer.announcedAddress).escapeHTML());
                }
                $.ajax({type: 'GET', url: 'http://peerexplorer.com/api_plugin_peerlist_json', async: false, contentType: "application/json", dataType: 'json', timeout: 60000,
                    success: function (json) {
                        for (i = 0; i < json.peerexplorer.length; i++) {
                            host = String(json.peerexplorer[i].host).escapeHTML();
                            rows += "<tr>";
                            rows += "<td>" + countPeers + "</td>";
                            rows += "<td>";
                            rows += (connectedPeers.indexOf(String(json.peerexplorer[i].host).escapeHTML()) > -1 ?
                                    "<i class='fa fa-check-circle' style='color:#5cb85c' title='Connected'></i>" : "<i class='fa fa-times-circle' style='color:#f0ad4e' title='Disconnected'></i>");
                            rows += "&nbsp;&nbsp;" + (String(json.peerexplorer[i].host).escapeHTML() ? String(json.peerexplorer[i].host).escapeHTML() : "No name");
                            rows += "&nbsp;&nbsp;" + (String(json.peerexplorer[i].api).escapeHTML() == 'true' ? "<i class='fa fa-sitemap' style='color:#3c8dbc' title='OpenAPI'></i>" : "") + "</td>";
                            rows += "<td>" + String(json.peerexplorer[i].ip).escapeHTML() + "</td>";
                            rows += "<td>" + String(json.peerexplorer[i].isp).escapeHTML() + "</td>";
                            rows += "<td>" + String(json.peerexplorer[i].platform).escapeHTML() + "</td>";
                            rows += "<td" + (String(json.peerexplorer[i].weight).escapeHTML() > 0 ? " style='font-weight:bold'" : "") + ">" + NRS.formatWeight(String(json.peerexplorer[i].weight).escapeHTML()) + "</td>";
                            rows += "<td><span class='label label-" + (NRS.versionCompare(String(json.peerexplorer[i].version).escapeHTML(), versionToCompare) >= 0 ? "success" : "danger") + "'>";
                            rows += (String(json.peerexplorer[i].version).escapeHTML() ? String('NRS ').escapeHTML() + " " + String(json.peerexplorer[i].version).escapeHTML() : "?") + "</label></td>";
                            rows += "<td>" + String(json.peerexplorer[i].country).escapeHTML() + "</td>";
                            rows += "<td>" + String(json.peerexplorer[i].lastcheck).escapeHTML() + "</td>";
                            rows += "<td style='text-align:right;'>";
                            rows += (String(json.peerexplorer[i].weight).escapeHTML() > 0 && String(json.peerexplorer[i].account).escapeHTML() != 0 ? "<a class='btn btn-xs btn-default' href='#' data-toggle='modal' "
                                    + "data-target='#send_money_modal' " + (NRS.needsAdminPassword ? "disabled " : "") + "data-recipient='" + String(json.peerexplorer[i].account).escapeHTML() + "'>"
                                    + $.t("Donate") + "</a>" : "");
                            if (NRS.needsAdminPassword) {
                            } else {
                                if (connectedPeers.indexOf(String(json.peerexplorer[i].host).escapeHTML()) == -1) {
                                    rows += "<a class='btn btn-xs btn-default' href='#' ";
                                    rows += "onClick='NRS.p_peerexplorer_connectPeer(\"" + String(json.peerexplorer[i].host).escapeHTML() + "\");'>";
                                    rows += $.t("Connect") + "</a>";
                                } else if (connectedPeers.indexOf(String(json.peerexplorer[i].host).escapeHTML()) > -1) {
                                    rows += "<a class='btn btn-xs btn-default' href='#' ";
                                    rows += "data-toggle='modal' data-target='#blacklist_peer_modal' data-peer='"
                                            + String(json.peerexplorer[i].host).escapeHTML() + "'>" + $.t("Blacklist") + "</a>";
                                }
                            }
                            rows += "</td>";
                            rows += "</tr>";
                            countPeers++;
                        }
                    },
                    error: function (json) {
                        $.growl("Cant connect to PeerExplorer API [peer list]", {
                            "type": "danger"
                        });
                    }
                });
                $.ajax({type: 'GET', url: 'http://peerexplorer.com/api_plugin_info_json', async: false, contentType: "application/json", dataType: 'json', timeout: 60000,
                    success: function (json) {
                        currentlyOnline = String(json.info[0].currentlyOnline).escapeHTML();
                        averageOnline = String(json.info[0].averageOnline).escapeHTML();
                        averageResponseTime = String(json.info[0].averageResponseTime).escapeHTML();
                        version = String(json.info[0].version).escapeHTML();
                        versionUsed = String(json.info[0].versionUsed).escapeHTML();
                    },
                    error: function (json) {
                        $.growl("Cant connect to PeerExplorer API [peer info]", {
                            "type": "danger"
                        });
                    }
                });
                $("#p_peerexplorer_currently_online").html(currentlyOnline).removeClass("loading_dots");
                $("#p_peerexplorer_average_online").html(averageOnline).removeClass("loading_dots");
                $("#p_peerexplorer_response_time").html(averageResponseTime).removeClass("loading_dots");
                $("#p_peerexplorer_version_used").html(version + ' (' + versionUsed + ')').removeClass("loading_dots");
                NRS.dataLoaded(rows);
            } else {
                $("#p_peerexplorer_currently_online, #p_peerexplorer_average_online, #p_peerexplorer_response_time, #p_peerexplorer_version_used").html("0").removeClass("loading_dots");
                NRS.dataLoaded();
            }
        });
    }

    NRS.forms.blacklistPeerComplete = function (response, data) {
        if (NRS.currentPage == 'p_peerexplorer') {
            $.growl($.t("success_blacklist_peer"), {
                "type": "success"
            });
            NRS.loadPage("p_peerexplorer");
        } else {
            NRS.loadPage("peers");
        }
    }

    $("#send_money_modal").on("show.bs.modal", function (e) {
        if (NRS.currentPage == 'p_peerexplorer') {
            var $invoker = $(e.relatedTarget);
            $("#send_money_recipient").html($invoker.data("recipient"));
            $("#send_money_recipient").val($invoker.data("recipient"));
        }
    });

    return NRS;
}(NRS || {}, jQuery));

function p_peerexplorer_refresh() {
    if (NRS.currentPage == 'p_peerexplorer') {
        NRS.loadPage("p_peerexplorer");
    }
}

setTimeout(p_peerexplorer_refresh, 4000);
setInterval(p_peerexplorer_refresh, 20000);